import datetime
import dateutil.parser
import time

class Runtime(object):
    def __init__(self):
        self.start_time = time.time()
        self.stop_time = None
        self.runtime = ''

    def _calculate_runtime(self):
        seconds = int(self.stop_time - self.start_time)
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)

        if h > 0:
            if h == 1:
                self.runtime += '{0} hour, '.format(h)
            else:
                self.runtime += '{0} hours, '.format(h)

        if m > 0 or (h > 0 and m == 0):
            if m == 1:
                self.runtime += '{0} minute and '.format(m)
            else:
                self.runtime += '{0} minutes and '.format(m)

        if s == 1:
            self.runtime += '{0} second'.format(s)
        else:
            self.runtime += '{0} seconds'.format(s)

        return self.runtime

    def stop(self):
        self.stop_time = time.time()

        # Calculate runtime
        self._calculate_runtime()

        return self.runtime


# Function to calculate a runtime
def calculate_runtime(start_time, end_time):
    runtime = ''
    seconds = int(end_time - start_time)
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    if h > 0:
        if h == 1:
            runtime += '{0} hour, '.format(h)
        else:
            runtime += '{0} hours, '.format(h)

    if m > 0 or (h > 0 and m == 0):
        if m == 1:
            runtime += '{0} minute and '.format(m)
        else:
            runtime += '{0} minutes and '.format(m)

    if s == 1:
        runtime += '{0} second'.format(s)
    else:
        runtime += '{0} seconds'.format(s)

    return runtime


# Function to convert string to date time object
def convert_string_to_datetime(date_string):
    datetime_obj = None

    # Try to remove the parse the string to a datetime object and
    # and remove the timezone info
    try:
        datetime_obj = dateutil.parser.parse(date_string)
        datetime_obj = datetime_obj.replace(tzinfo=None)
    except Exception as e:
        return date_string
    finally:
        # Return the datetime object, can be either a datetime or None object
        return datetime_obj


# Function to convert datetime to string
def convert_datetime_to_string(datetime_value, format):
    if format == 'Jira':
        return datetime_value.strftime('%Y-%m-%d %H:%M')
    elif format == 'Dutch':
        return datetime_value.strftime('%d-%m-%Y')
    else:
        raise ValueError('Unknown Format')


def date_range(start_date, end_date, whole_weeks=True, reverse=False):
    """Daterange

    Arguments:
        start_date {date/datetime} -- Start date of the range
        end_date {date/datetime} -- End date of the range
        whole_weeks {boolean} -- the start date will be rounded up to mondays,
                                and the end date will be rounded up to sundays

    Returns a tuple:
        date (date/datetime) -- the date
        weekday (integer) -- the day number in week
        weeknumber
    """
    if whole_weeks:
        # A week starts at day monday, see if starts otherwise get the date of monday
        if not start_date.isocalendar()[2] == 1:
            diff = int(start_date.isocalendar()[2]) - 1
            start_date -= datetime.timedelta(days=diff)

        # A week ends on sunday, see if it ends there, otherwise get the date of sunday
        if not end_date.isocalendar()[2] == 7:
            diff = 7 - int(end_date.isocalendar()[2])
            end_date += datetime.timedelta(days=diff)

    date_list = list()
    for i in range((end_date - start_date).days + 1):
        date_item = start_date + datetime.timedelta(days=i)
        iso_date = date_item.isocalendar()
        weeknumber = iso_date[1]
        weekday = iso_date[2]
        date_list.append((date_item, weekday, weeknumber))

    if reverse:
        return list(reversed(date_list))
    return date_list
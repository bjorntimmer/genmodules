import sys

# Function to get the credentials
def get_credentials(credentialsfile):
    try:
        with open(credentialsfile, 'r') as file:
            username, password = file.read().split(',')
    except FileNotFoundError as error:
        errormessage = 'The credentials file "{0}" could not be opened. The error message is: "{1}"'.format(credentialsfile, error)
        sys.exit(errormessage)

    return username, password
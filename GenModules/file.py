import csv
import json
import os
import sys

def create_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

# List files
def files_in_directory(listing_directory):
    directory = os.listdir(listing_directory)
    file_list = list()
    for file_obj in directory:
        if os.path.isfile(os.path.join(listing_directory, file_obj)):
            file_list.append(file_obj)
    return file_list

# Read CSV to a dictionary
def read_csv_to_dict(csv_file=None, delimiter=';', header_fields=None):
    csv.register_dialect('csv_reader', delimiter=delimiter)
    with open(csv_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile, dialect='csv_reader')
        csv_list = list()

        # Check the header
        if header_fields:
            if reader.fieldnames != header_fields:
                sys.exit('Invalid CSV fieldnames')

        # Read the rows into dictionaries and put them into a list
        for row in reader:
            csv_row = dict(row)
            csv_list.append(dict(row))

        csvfile.close()

    return csv_list


# Read json from file
def read_json_from_file(json_input):
    with open(json_input, 'r') as json_file:
        json_data = json.loads(json_file.read())

    return json_data


# Write data to csv
def write_csv(output_file, data, header):
    with open(os.path.join(os.getcwd(), output_file), 'w') as csv_file:
        # Create the csv writer and write the header
        csv_writer = csv.writer(csv_file, delimiter=';', lineterminator='\n', quotechar='"', quoting=csv.QUOTE_ALL)

        # Write the header
        csv_writer.writerow(header)

        for data_row in data:
            csv_writer.writerow(data_row)


# Write output to json
def write_json(output_file, data):
    with open(os.path.join(os.getcwd(), output_file), 'w') as json_output_file:
        json.dump(data, json_output_file, indent=4, sort_keys=True)
def cast_boolean(value):
    if value.lower() == 'true':
        return True
    elif value.lower() == 'false':
        return False
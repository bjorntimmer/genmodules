import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import os
import time
import random

class MailClient(object):
    def __init__(self, smtp_server='smtp.kpnnl.local', smtp_port='25'):
        self._server = smtp_server
        self._port = smtp_port
        self._from_address = None
        self._to_address = None
        self._cc_address = None
        self._bcc_address = None
        self._subject = None
        self._body = None
        self._attachements = None

        self._msg = MIMEMultipart()


    def _add_msg_date(self):
        self._msg['Date'] = formatdate(localtime=True)


    def _add_msg_header(self):
        self._msg.add_header('Message-ID', self._generate_message_id(self.from_address))


    def _generate_message_id(self, msg_from):
        domain = msg_from.split("@")[1]
        r = "%s.%s" % (time.time(), random.randint(0, 100))
        mid = "<%s@%s>" % (r, domain)

        return mid


    @property
    def from_address(self):
        return self._from_address


    @from_address.setter
    def from_address(self, str_value):
        assert type(str_value) == str

        self._from_address = str_value
        self._msg['From'] = self._from_address


    @property
    def to_address(self):
        return self._to_address


    @to_address.setter
    def to_address(self, list_value):
        assert type(list_value) == list

        self._to_address = list_value
        self._msg['To'] = COMMASPACE.join(self._to_address)


    @property
    def cc_address(self):
        return self._cc_address


    @cc_address.setter
    def cc_address(self, list_value):
        assert type(list_value) == list
        self._cc_address = list_value
        self._msg['Cc'] = COMMASPACE.join(self._cc_address)


    @property
    def bcc_address(self):
        return self._bcc_address


    @bcc_address.setter
    def bcc_address(self, str_value):
        assert type(str_value) == list

        self._bcc_address = str_value


    @property
    def subject(self):
        return self._subject


    @subject.setter
    def subject(self, str_value):
        assert type(str_value) == str

        self._subject = str_value
        self._msg['Subject'] = self._subject


    @property
    def body(self):
        return self._body


    @body.setter
    def body(self, str_value):
        assert type(str_value) == str

        self._body = str_value
        self._body = self._body.encode("utf-8")
        self._body = MIMEText(self._body, 'plain', "utf-8")
        self._msg.attach(self._body)


    @property
    def attachements(self):
        return self._attachements


    @attachements.setter
    def attachements(self, list_value):
        assert type(list_value) == list

        self._attachements = list_value
        for file in list_value:
            part = MIMEBase('application', "octet-stream")
            part.set_payload( open(file,"rb").read() )
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"'
                           % os.path.basename(file))
            self._msg.attach(part)


    def send(self):
        smtp = smtplib.SMTP(self._server)
        self._add_msg_date()
        self._add_msg_header()

        if self._cc_address and self._bcc_address:
            smtp.sendmail(
                self._from_address,
                [self._to_address, self._cc_address] + self._bcc_address,
                self._msg.as_string()
                )
        elif self._cc_address:
            smtp.sendmail(
                self._from_address,
                [self._to_address, self._cc_address],
                self._msg.as_string()
                )
        elif self._bcc_address:
            smtp.sendmail(
                self._from_address,
                [self._to_address] + self._bcc_address,
                self._msg.as_string()
                )
        else:
            smtp.sendmail(
                self._from_address,
                [self._to_address],
                self._msg.as_string()
                )
        smtp.close()

        return self._msg
import os
from distutils.core import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

setup(
    name='GenModules',
    version='1.0',
    packages=['GenModules'],
    url='',
    author='Bjorn Timmer',
    author_email='bjorn@timmer.pro',
    description='General functions to make life easier',
    install_requires=['python-dateutil']
)